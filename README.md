# REView BAMs

This is a Python script for creating read visualisations for each STR locus in BED regions file for a given BAM/CRAM file.

The tab-separated input BED file has to contain at least four columns with reference coordinates and the repeat unit (motif). Fifth and sixth column (Locus ID and Genotype) are optional. Custom genotypes can be used without specifying Locus ID by leaving an empty Locus ID column (i.e. leave two tabs between Motif and Genotype). If using custom genotypes then they will be applied for all input samples. A column header should be omitted from the file.

| Chromosome | Start     | End      | Motif | Locus ID | Genotype |
| ---------- | --------- | -------- | ----- | -------- | -------- |
| chrX       | 67545316  | 67545385 | GCA   |          |          |
| chr4       | 3074876   | 3074933  | CAG   | HTT      |          |
| chr9       | 69037286  | 69037304 | GAA   | FXS      |          |
| chr14      | 92071010  | 92071040 | CTG   | ATXN3    | 18/26    |

-------

**The script can take the following arguments.**

| Argument                   | Required   | Default         | Comment                                             |
| -------------------------- | ---------- | --------------- | --------------------------------------------------- |
| \--input                   | Yes        |                 | Input folder or comma separated list of BAM files   |
| \--output                  | Yes        |                 | Output folder where alignment files will be stored  |
| \--regions                 | Yes        |                 | BED file with reference coordinates and repeat unit |
| \--reference               | Yes        |                 | FASTA file of the reference genome                  |
| \--html                    | *No*       | false           | Generate HTML file? Options: true, false            |
| \--expansionhunter         | *No*       | ExpansionHunter | Path to ExpansionHunter executable file             |
| \--reviewer                | *No*       | REViewer        | Path to REViewer executable file                    |
| \--samtools                | *No*       | samtools        | Path to Samtools executable file                    |

*If ExpansionHunter, REViewer and Samtools executables exist in `$PATH` then their respective arguments are not required.*

-------

### Example usage
You can run the script on all files in the example/ folder by using the following command (assuming that all tools are in one of your folders specified in `$PATH`):

`python3 visualise.py --input example/ --output example/output/ --regions example/regions.bed --reference /path/to/hg38.fa`


When using a single sample, use: `--input example/sample001.bam` or if multiple, then separate samples with a comma, such as: `--input example/sample001.bam,example/sample002.bam`


The output of the script is SVG file(s) that will be saved into the output folder followed by the `--output` argument. In order to view all generated visualisations (SVG files) in an easy way, then you can specify `--html true` which will create a HTML file into the output folder that can be opened in your browser displaying all visualisations for each sample and locus.

-------

If any of the tools (ExpansionHunter, REViewer and Samtools) is not accessible through the command line (i.e. it is not in your `$PATH`) then you need to specify the full path of each tool's executables. For example, if you have Samtools installed and it is accessible through the command line (e.g. you get a response by entering `samtools --version`), but ExpansionHunter and REViewer are not installed, then you need to specify the path for the latter tools separately, such as:

`python3 visualise.py --input example/ --output example/output/ --regions example/regions.bed --reference /path/to/hg38.fa --expansionhunter /path/to/expansionhunter --reviewer /path/to/reviewer`
