import os
import csv
import json
import glob
import logging
import argparse
import datetime
import tempfile
import subprocess


def programExists(executable):
	"""Checks whether an executable file exists by running it with --version argument

	Args:
		executable (str): executable file path or name if in $PATH

	Returns:
		bool: True if exists and False if not
	"""

	return True if subprocess.getstatusoutput([executable, '--version'])[0] != 127 else False

def collectInputFiles(input, checkFiles = False):
	"""Put all input files into an array

	Args:
		input (str): Input files or folder
		checkFiles (bool): Specify whether to check the presence of input files

	Returns:
		array: All input files separately in an array
	"""

	if os.path.isdir(input):
		allowed_file_extensions = ['*.bam', '*.cram']
		input_files = [f for ext in allowed_file_extensions for f in glob.glob(os.path.join(args.input, ext))]
	else:
		if "," in args.input:
			input_files = args.input.split(",")
		else:
			input_files = [args.input]

	# Check that all files actually do exist
	if checkFiles:
		filesPresent = True
		for file in input_files:
			if not os.path.exists(file):
				filesPresent = False
		return filesPresent

	return input_files

def prerequisitesMet(args):
	"""Goes through all files and folders and checks whether they exist

	Args:
		args (args): Input arguments

	Returns:
		bool: True if all prerequisites are met and False if at least one was not
	"""

	error = False

	if not collectInputFiles(args.input, checkFiles = True):
		logger.error("Some of the input files were not found: " + args.input)
		error = True

	if not os.path.isdir(args.output):
		logger.error("Output folder does not exist: " + args.output)
		error = True

	if not os.path.exists(args.reference):
		logger.error("Reference FASTA file was not found: " + args.reference)
		error = True

	if not os.path.exists(args.regions):
		logger.error("BED file was not found: " + args.regions)
		error = True

	if not programExists(args.expansionhunter):
		logger.error("ExpansionHunter's executable was not found: " + args.expansionhunter)
		error = True

	if not programExists(args.reviewer):
		logger.error("REViewer's executable was not found: " + args.reviewer)
		error = True

	if not programExists(args.samtools):
		logger.error("Samtools's executable was not found: " + args.samtools)
		error = True

	return True if not error else False


def createHTML(output_folder, rows):
	"""Creates a HTML file for easy viewing results in a browser

	Args:
		output_folder (str): Output folder where file will be saved
		rows (str): Rows containing analysed results which are added to the html table

	Returns:
		bool: True if HTML file was created successfully and False if not
	"""
	
	html = """
				<!doctype html>
				<html lang="en">
				<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1">
					<title>REViewer - results</title>
					<style>body{margin:0;font-family:monospace,monospace;font-size:1em}table{border-collapse:collapse;border-spacing:0;empty-cells:show;border:1px solid #cbcbcb}table td,table th{border-left:1px solid #cbcbcb;border-width:0 0 0 1px;font-size:inherit;margin:0;overflow:visible;padding:.5em 1em;background-color:transparent;border-bottom:1px solid #cbcbcb}table thead{background-color:#e0e0e0;color:#000;text-align:center;vertical-align:middle}table tr:nth-child(2n-1) td{background-color:#f2f2f2}table tbody{text-align:center;white-space:nowrap;font-size:.9em}img{max-width:100%;height:auto;display:block}</style>
				</head>
				<body>
					<table>
						<thead>
							<tr>
								<th>Sample and locus</th>
								<th>Visualisation</th>
							</tr>
						</thead>
						<tbody>
							""" + rows + """
						</tbody>
					</table>
				</body>
				</html>
			"""

	html_file_name = "results_" + datetime.datetime.now().strftime("%Y-%m-%d_%H%M") + ".html"
	html_file_path = os.path.join(output_folder, html_file_name)
	html_file = open(html_file_path, "w")
	html_file.write(html)
	html_file.close()
	
	return html_file_name if os.path.exists(html_file_path) else False

def createHTMLtableRows(input_file, variant, motif, coordinates, sample_name):
	"""Creates rows for HTML table

	Args:
		input_file (str): Input file name (without full path but with extension)
		variant (str): Locus ID or coordinates if missing
		motif (str): Motif
		coordinates (str): Reference coordinates
		sample_name (str): Sample name (without file extension)

	Returns:
		str: New table row containing variant data and the image
	"""

	svg_file_name = sample_name + "." + variant + ".svg"

	row = """
			<tr>
				<td>
					<h3>""" + input_file + """</h3>
					<h4>Locus """ + variant + """</h4>
					<u>""" + motif + """</u>@""" + coordinates + """
				</td>
				<td>
					<a href=\"""" + svg_file_name + """\"><img src=\"""" + svg_file_name + """\"/></a>
				</td>
			</tr>
		"""
	
	return row


def createErrorSVG(output_file, variant, sample_file):
	"""Creates SVG file containing an error

	Args:
		output_file (str): Output SVG file path
		variant (str): Locus ID or coordinates if missing
		sample_name (str): Sample name (without file extension)

	Returns:
		bool: True if SVG file was created successfully and False if not
	"""

	svg_file = open(output_file, "w")
	svg_file.write("""
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" height="200" width="800">
							<text x="0" y="15" fill="red">Error: Visualisations could not be created for the """ + variant + """ locus in sample """ + sample_file + """</text>
						</svg>
					""")
	svg_file.close()
	
	return True if os.path.exists(output_file) else False


def writeNewVCF(vcf_file_path, content):
	"""Overwrites the ExpansionHunter's outputted VCF file and replaces genotypes with user specified ones

	Args:
		vcf_file_path (str): VCF file full path
		content (array): VCF file new content - each locus line as an array element

	Returns:
		bool: True if file exists
	"""

	with open(vcf_file_path, "w", newline = '\n') as new_vcf_file:
		vcfwriter = csv.writer(new_vcf_file, delimiter = '\t')
		
		for line in content:
			vcfwriter.writerow(line)

	return True if os.path.exists(vcf_file_path) else False


def main():
	"""Takes all input files (or files in specified folder) and runs ExpansionHunter on all loci specified in the regions file which outputted files are used to create read visualisations with REViewer.
	"""

	input_files = collectInputFiles(args.input)

	# Create temporary folder to store intermediate files
	with tempfile.TemporaryDirectory() as tempdir:
		html_table_rows = ''

		for file in input_files:
			# Set file name (with extension), sample name (without extension) and temporary file path
			file_name = os.path.basename(file)
			sample_name = os.path.splitext(file_name)[0]
			temp_file = os.path.join(tempdir, sample_name)

			# Create variant catalogue from regions file
			variants = []
			specified_genotypes = {}
			temp_variant_catalogue = os.path.join(tempdir, 'variant_catalog.json')

			with open(args.regions, newline = '\n') as bedfile:
				regions = csv.reader(bedfile, delimiter = '\t')
				for region in regions:
					if not region:
						continue

					reference_region = region[0].strip() + ":" + region[1].strip() + "-" + region[2].strip()
					locus_id = region[4].strip() if len(region) >= 5 and region[4] != "" else reference_region.replace(":", "-")
					genotype = region[5].strip() if len(region) >= 6 else ''
					motif = region[3].strip()

					# If user has specified a genotype for the locus, then add this to dictionary and create homozygous genotype when only one value is specified
					if genotype:
						genotype_insert = genotype if "/" in genotype else (genotype + "/" + genotype)
						specified_genotypes.update({locus_id: genotype_insert})

					variants.append({
						"LocusId": locus_id,
						"LocusStructure": "(" + motif + ")*",
						"ReferenceRegion": reference_region,
						"VariantType": "Repeat"
					})

			with open(temp_variant_catalogue, 'w') as outfile:
				json.dump(variants, outfile, indent = 4)

			# Call ExpansionHunter
			logger.info("Genotyping sample " + file_name + " with ExpansionHunter")
			subprocess.call([args.expansionhunter,	"--reads", file, 
													"--reference", args.reference, 
													"--variant-catalog", temp_variant_catalogue, 
													"--output-prefix", temp_file],
							stdout = subprocess.DEVNULL)

			# If ExpansionHunter's output file was not found then log and take the next sample
			if not os.path.exists(temp_file + "_realigned.bam"):
				logger.warning("ExpansionHunter failed to run on " + file_name + " with the selected parameters")
				continue

			eh_vcf_file_path = temp_file + ".vcf"

			# Samtools sort and index
			logger.info("Sorting and indexing " + file_name + " with Samtools")
			subprocess.call([args.samtools, "sort", "-o", temp_file + "_realigned-sorted.bam", temp_file + "_realigned.bam"], stdout = subprocess.DEVNULL)
			subprocess.call([args.samtools, "index", temp_file + "_realigned-sorted.bam"], stdout = subprocess.DEVNULL)

			# If Samtools sorted and indexed files were not found then log and take the next sample
			if not os.path.exists(temp_file + "_realigned-sorted.bam") and os.path.exists(temp_file + "_realigned-sorted.bam.bai"):
				logger.warning("Samtools failed to run on " + file_name)
				continue
			
			# Replace the intermediate VCF file with user specified genotype(s) if they are used
			if specified_genotypes:
				eh_vcf_file = open(eh_vcf_file_path, "r")

				new_vcf_content = []

				for line in eh_vcf_file:
					if not line.startswith("#"):
						line = line.rstrip()
						line_locus_id = line.split("\t")[7].partition("VARID=")[2].partition(";REPID")[0]
						line_locus_genotype = line.split("\t")[9].split(":")[2]

						if line_locus_id in specified_genotypes:
							if specified_genotypes[line_locus_id] != line_locus_genotype:
								new_locus_line = line.split("\t")[0:9] + ['0/0:0/0:' + specified_genotypes[line_locus_id] + ':0-0/0-0:0/0:0/0:0/0:0']
							else:
								new_locus_line = line.split("\t")
						else:
							new_locus_line = line.split("\t")

						new_vcf_content.append(new_locus_line)

				eh_vcf_file.close()

				eh_vcf_file_path_replaced = temp_file + ".replaced.vcf"
				vcf_replaced = writeNewVCF(eh_vcf_file_path_replaced, new_vcf_content)

				if vcf_replaced:
					logger.info("Custom genotype(s) set for sample " + file_name)
					eh_vcf_file_path = eh_vcf_file_path_replaced
				else:
					logger.error("Failed to create custom genotype(s) for sample " + file_name)

			# Run REViewer on each variant
			for variant in variants:
				variant_id = variant["LocusId"]
				variant_coord = variant["ReferenceRegion"]
				variant_motif = variant["LocusStructure"].partition("(")[2].partition(")")[0]
				output_file = os.path.join(args.output, sample_name)

				logger.info("Creating visualisations for " + variant_id + " locus in " + file_name + " with REViewer")

				# Run REViewer on each locus
				subprocess.call([args.reviewer,	"--reads", temp_file + "_realigned-sorted.bam", 
												"--reference", args.reference, 
												"--catalog", temp_variant_catalogue, 
												"--vcf", eh_vcf_file_path,
												"--locus", variant_id,
												"--output-prefix", output_file],
								stdout = subprocess.DEVNULL)

				html_table_rows += createHTMLtableRows(file_name, variant_id, variant_motif, variant_coord, sample_name)

				# If the SVG was not created then generate a SVG with an error and log it
				if not os.path.exists(output_file + "." + variant_id + ".svg"):
					createErrorSVG(output_file + "." + variant_id + ".svg", variant_id, sample_name)
					logger.warning("REViewer could not create visualisations for the " + variant_id + " locus in " + file_name)
					continue		
	
			logger.info("Visualisations created for " + variant_id + " locus in " + file_name)
		
		if args.html == "true":
			html_created = createHTML(args.output, html_table_rows)

			if html_created:
				logger.info("HTML file created: " + html_created)
			else:
				logger.error("HTML table could not be created")
			

if __name__ == '__main__':
	# Accept arguments from command line
	parser = argparse.ArgumentParser()
	parser.add_argument("--input",						required = True, 																		help = "Input folder containing the BAM files or comma separated list (e.g. sample1.bam,sample2.bam)")
	parser.add_argument("--output",						required = True, 																		help = "Output folder where alignment files will be stored")
	parser.add_argument("--regions",					required = True, 																		help = "BED file with reference coordinates and repeat unit")
	parser.add_argument("--reference",					required = True, 																		help = "FASTA file of the reference genome")
	parser.add_argument("--html",						required = False, default = "false", 		choices = ["true", "false"],				help = "Create HTML file to view alignments? Default is 'false'")
	parser.add_argument("--expansionhunter",			required = False, default = "ExpansionHunter",											help = "Path to ExpansionHunter executable file")
	parser.add_argument("--reviewer",					required = False, default = "REViewer",													help = "Path to REViewer executable file")
	parser.add_argument("--samtools",					required = False, default = "samtools",													help = "Path to Samtools executable file")

	args = parser.parse_args()

	# Create the logger
	logger = logging.getLogger(__name__) 
	logging.basicConfig(level = logging.INFO)

	# Run the process if all prerequisites are met
	if prerequisitesMet(args):
		logger.info("All requirements were met, starting the analysis...")
		main()
